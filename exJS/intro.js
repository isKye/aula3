console.log("Hello World!");
let variavel = -5;
console.log("variável " + variavel + " é do tipo: " + typeof (variavel));

//operadores matemáticos

let num1 = 5;
let num2 = 10;

console.log(num1 + num2);
console.log(5 * 2);

let idade = 40;

if (idade == undefined) {
    console.log("Idade não foi criada!");
} else {
    console.log("Sua idade é: " + idade);
}

/*teste de erro

try {
    let idade = 40;

    if (idade == undefined) {
        console.log("Idade não foi criada!");
    } else {
        console.log("Sua idade é: " + idade);
    }
}catch(error){
    console.log("Erro na variável idade!");
}

*/

function mostrarIdade(){
    let idade = 40;
    if(idade < 41){
        console.log("Jovem");
    }else{
        console.log("Não muito jovem");
    }
}

mostrarIdade();

function mostrarIdade2(vlIdade){
    if(vlIdade < 42){
        console.log(vlIdade + " - Jovem");
    }else{
        console.log(vlIdade + " - Não muito jovem");
    }
}

mostrarIdade2(42);
mostrarIdade2(40);

const mostrarIdade3 = (vlIdade) => {
    if(vlIdade < 42){
        return vlIdade + " - Jovem - Versão 3";
    }else{
        return vlIdade + " - Não muito jovem - Versão 3";
    }
}

console.log(mostrarIdade3(42));